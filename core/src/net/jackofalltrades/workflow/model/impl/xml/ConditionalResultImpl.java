package net.jackofalltrades.workflow.model.impl.xml;

import net.jackofalltrades.workflow.model.xml.ConditionalResult;

public abstract class ConditionalResultImpl extends CommonWorkflowElement implements ConditionalResult {

    @Override
    public void accept(Visitor visitor) {
        visitor.visitConditionalResult(this);
    }

}
